import React,{useState,useEffect } from 'react';
import Taro from '@tarojs/taro'
import styles from './index.module.scss';
import {View, Button, Editor,Icon,Text,Swiper,SwiperItem} from '@tarojs/components';
// import styles from "@components/thread-post/title/index.module.scss";
import {handleHtmlImage} from "@components/taro-editor/util";
import classNames from "classnames";
import locals from "../../../../common/utils/local-bridge";
import constants from '@common/constants';
import {inject} from "mobx-react";

@inject('site')
class TaroEditor extends React.Component {
  constructor() {
    super();
    this.state = {
      showImgSize:false,
      showImgToolbar:false,
      showImgResize:false,
      content:'',
      placeholder:'请输入内容',

      tempFilePaths:[],
      uploadTask:{},
      file_index:0,

      scrollViewHeight:0,
      scrollHeight:0,
      fullToolBarHeight:0,

      isFixed: true,
      iconArray:['icon-add', 'icon-textformat', 'icon-align-left'],
      fixedBottom:0,
      iphoneXBottomH: 0,
      scrollHeightDefault: 0,
      keyboardHeight: 0,
      readOnly: true,
      isDefaultFormat: true, // 首次聚集时设置默认格式
      isIos: false,
      inputFocus: false,
      formats: {},
      formatArray: [{
        type: 'feature',
        array: [{
          name: 'chooseImage',
          icon: 'image'
        },
          // {
          //   name: 'chooseImagebyCamera',
          //   icon: 'photo'
          // },
          {
            name: 'insertDivider',
            icon: 'line'
          }
        ]
      },
        {
          type: 'tool',
          array: [{
            name: 'text',
            label: 'icon',
            items: [{
              name: 'bold',
              icon: 'bold'
            },
              {
                name: 'italic',
                icon: 'italic'
              },
              {
                name: 'underline',
                icon: 'underline'
              },
              {
                name: 'strike',
                icon: 'strikethrough'
              },
              {
                name: 'backgroundColor',
                value: 'yellow',
                icon: 'fontbgcolor'
              }
            ]
          },
            {
              name: 'defaultFormat',
              items: [{
                title: '标题',
                format: {
                  fontSize: '18px',
                  bold: 'strong'
                },
                style: {
                  fontSize: '18px',
                  fontWeight: 'bold'
                }
              },
                {
                  title: '小标题',
                  format: {
                    fontSize: '16px',
                    bold: 'strong'
                  },
                  style: {
                    fontSize: '16px',
                    fontWeight: 'bold'
                  }
                },
                {
                  title: '正文',
                  format: {
                    fontSize: '14px'
                  },
                  style: {
                    fontSize: '14px'
                  }
                },
                {
                  title: '注释',
                  format: {
                    fontSize: '12px',
                    color: '#888888'
                  },
                  style: {
                    fontSize: '12px',
                    color: '#888888',
                  }
                }
              ]
            },
            {
              name: 'fontSize',
              items: [{
                title: '18',
                value: '18px'
              },
                {
                  title: '16',
                  value: '16px'
                },
                {
                  title: '14',
                  value: '14px'
                },
                {
                  title: '12',
                  value: '12px'
                },
                {
                  title: '11',
                  value: '11px'
                },
                {
                  title: '10',
                  value: '10px'
                }
              ]
            },
            {
              name: 'color',
              items: [{
                value: '#000000'
              },
                {
                  value: '#888888'
                },
                {
                  value: '#ffffff'
                },
                {
                  value: '#f6de41'
                },
                {
                  value: '#f68c41'
                },
                {
                  value: '#fd3136'
                },
                {
                  value: '#5ad8a6'
                }
              ]
            }
          ]
        },
        {
          type: 'tool',
          array: [{
            name: 'align',
            label: 'icon',
            items: [{
              value: 'left',
              icon: 'align-left'
            },
              {
                value: 'center',
                icon: 'align-center'
              },
              {
                value: 'right',
                icon: 'align-right'
              }
            ]
          },
            {
              name: 'text',
              label: 'icon',
              items: [{
                name: 'list',
                value: 'ordered',
                icon: 'orderedlist'
              },
                {
                  name: 'list',
                  value: 'bullet',
                  icon: 'unorderedlist'
                },
                {
                  name: 'indent',
                  icon: 'outdent',
                  value: '+1'
                },
                {
                  name: 'indent',
                  icon: 'indent',
                  value: '-1'
                }
              ]
            },
            {
              name: 'lineHeight',
              items: [{
                value: 1
              },
                {
                  value: 1.3
                },
                {
                  value: 1.5
                },
                {
                  value: 2
                },
                {
                  value: 3
                }
              ]
            }
          ]
        }
      ],
      curLength: 0,
      swiperCurrent: 0,
       toolbarShow: false,
      // toolbarShow: true,
      toolBarContentShow: false,
      fixedTopHeight: 0, // 顶部工具栏高度
      toolBarHeight: 100, // 工具栏高度
      toolBarContentHeight: 530, // 工具栏内容高度
      progress: true //判断是否监听上传进度变化

    }
  }
  componentDidMount() {

    const query = Taro.createSelectorQuery().in(this)
    query.select('#fixed-top').boundingClientRect(res => {
      // this.fixedTopHeight = res.height
      this.setState({
        fixedTopHeight:res.height
      })
    }).exec()

    const system = Taro.getSystemInfo({
      success: e => {
        // this.isIos = e.platform == 'ios'
        let isIphoneX = (e.platform == 'devtools' || this.isIos) && e.safeArea.top == 44
        this.setState({
          isIos:e.platform == 'ios',
          iphoneXBottomH:isIphoneX ? 34 : 0,
          scrollHeightDefault: e.windowHeight - 34
        },()=>{
            this.scrollViewHeight()
          // const _fixedBottom = this.fixedBottom()
          // console.log(_fixedBottom,'_fixedBottom2222---')
          // this.setState({
          //   fixedBottom:_fixedBottom
          // },()=>{
          //   const _scrollHeight = this.scrollHeight()
          //   console.log(_scrollHeight,'_scrollHeight3333---')
          //   this.setState({
          //     scrollHeight:_scrollHeight
          //   },()=>{
          //     const _scrollViewHeight =this.scrollViewHeight()
          //     console.log(_scrollViewHeight,'_scrollViewHeight44444444444---')
          //     this.setState({
          //       scrollViewHeight:_scrollViewHeight
          //     })
          //   })
          // })

        })

        // this.iphoneXBottomH = isIphoneX ? 34 : 0
        // this.scrollHeightDefault = e.windowHeight - 34
      }
    })

    Taro.onKeyboardHeightChange(res => {
      let keyboardHeight = this.state.keyboardHeight
      //重新计算scrollViewHeight
      this.scrollViewHeight()
      if(keyboardHeight>0){
        this.setState({
          toolBarContentShow:false
        })
      }
      if (res.height === keyboardHeight) return

      // this.keyboardHeight = res.height;
      this.setState({
        keyboardHeight:res.height
      })
      const duration = res.height > 0 ? res.duration * 1000 : 0
      keyboardHeight = res.height;
      if(keyboardHeight>0){
        this.setState({
          toolBarContentShow:false
        })
      }
      //重新计算scrollViewHeight
      this.scrollViewHeight()


      setTimeout(() => {
        Taro.pageScrollTo({
          scrollTop: 0,
          success: () => {
            this.updatePosition(keyboardHeight)
            this.editorCtx.scrollIntoView() //使得编辑器光标处滚动到窗口可视区域内
          }
        })
      }, duration)
    })


    // this.fixedBottom()


    // const _fixedBottom = this.fixedBottom()
    // const _scrollHeight = this.scrollHeight()
    // const _scrollViewHeight =this.scrollViewHeight()
    // this.setState({
    //   fixedBottom:_fixedBottom,
    //   scrollHeight:_scrollHeight,
    //   scrollViewHeight:_scrollViewHeight
    // })

    // console.log(_fixedBottom,'fixedBottom')
  }

  // fullToolBarHeight() {
  //   let height = 0
  //   this.state.toolbarShow ? height += this.state.toolBarHeight : ''
  //   this.state.toolBarContentShow ? height += this.state.toolBarContentHeight : ''
  //   return height
  // }

  fixedBottom() {
    return this.state.isIos || this.state.iphoneXBottomH > 0 ? (this.state.keyboardHeight > 0 ? this.state.keyboardHeight :
      this.state.iphoneXBottomH) : 0
  }
  scrollHeight() {
    let _fullToolBarHeight = 0
    this.state.toolbarShow ? _fullToolBarHeight += this.state.toolBarHeight : ''
    this.state.toolBarContentShow ? _fullToolBarHeight += this.state.toolBarContentHeight : ''
    return this.state.scrollHeightDefault - this.state.fixedTopHeight - _fullToolBarHeight;
    // return this.state.scrollHeightDefault - this.state.fixedTopHeight - this.state.fullToolBarHeight;
  }
  scrollViewHeight() {
    let _fullToolBarHeight = 0
    this.state.toolbarShow ? _fullToolBarHeight += this.state.toolBarHeight : ''
    this.state.toolBarContentShow ? _fullToolBarHeight += this.state.toolBarContentHeight : ''
    _fullToolBarHeight = _fullToolBarHeight/2
    console.log(_fullToolBarHeight,'_fullToolBarHeight1111111-----')
    let _scrollHeight =  this.state.scrollHeightDefault - this.state.fixedTopHeight - _fullToolBarHeight;


    let scrollViewHeight = _scrollHeight - this.state.keyboardHeight;
    // console.log(this.state.scrollHeight,'scrollViewHeight5555555555--------')
    // console.log(this.state.keyboardHeight,'scrollViewHeight66666666666-------')
    console.log(scrollViewHeight,'scrollViewHeight77777777777-------')
    scrollViewHeight =  this.state.keyboardHeight > 0 ? scrollViewHeight + this.state.iphoneXBottomH : scrollViewHeight;
    this.setState({
      scrollViewHeight:scrollViewHeight
    })
  }

  isActive(item, pitem) {
    let {
      name,
      value,
      format
    } = item
    !name ? name = pitem.name : ''
    if (format) {
      for (let name in format) {
        if (this.state.formats[name] !== format[name]) {
          return false
        }
      }
      return true
    } else {
      return value ? this.state.formats[name] === value : this.state.formats[name]
    }
  }

  hideKeyboard() {
    Taro.hideKeyboard() //uni-app提供了隐藏软键盘的api，但是没有生效
    // this.setState({
    //   toolBarContentShow:false
    // })
    this.scrollViewHeight()
    this.editorCtx.blur()
  }
  changeSwiper(current) {
    this.setState({
      toolBarContentShow:true,
      swiperCurrent:current
    },()=>{

      this.scrollViewHeight()
    })
    // this.toolBarContentShow = true
    // this.swiperCurrent = current
    this.hideKeyboard()
  }
  updatePosition(keyboardHeight) {
    // this.keyboardHeight = keyboardHeight
    console.log(keyboardHeight,'keyboardHeight1111----')
    this.setState({
      keyboardHeight:keyboardHeight
    })
    if(keyboardHeight>0){
      this.setState({
        toolBarContentShow:false
      })
    }
  }
  onEditorReady() {
    const that = this
    console.log('onEditorReady')
    Taro.createSelectorQuery()
      // .in(this)
      .select('#editor')
      .context(function(res) {
        that.editorCtx = res.context
        let _content = locals.get('_editorContent')
       // console.log(_activityInfoList,'_activityInfoList---')

        // that.setValue(that.state.content)
        that.setValue(_content || '')
        // console.log('ddddddddddd---')
        // //设置默认格式
        // // that.editorCtx.format('header', '4')
        // that.editorCtx.format('fontSize', '14px')
        // that.editorCtx.format('align', 'left')
        // that.editorCtx.format('lineHeight', '1.3')
        //setContents设置内容后editor会自动聚焦，解决：先设置read_only为true,赋值后再把read_only属性设置为false
        that.readOnly = false
      })
      .exec()
  }
  onEditorInput(e) {
    let {
      html,
      text
    } = e.detail
    console.log(html,'html----input')
    this.setState({
      curLength: text.length - 1
    })
    // this.curLength = text.length - 1
  }
  onEditorFocus(e) {
    this.setState({
      toolbarShow:true,
      toolBarContentShow:false,
      inputFocus:true
    },()=>{
      //scrollViewHeight
      this.scrollViewHeight()
    })
    // this.toolbarShow = true
    // this.inputFocus = true
    if (this.isDefaultFormat) {
      //设置默认格式
      this.editorCtx.format('fontSize', '14px')
      this.editorCtx.format('align', 'left')
      this.setState({
        isDefaultFormat:false
      })
      // this.isDefaultFormat = false
    }
  }
  onEditorBlur() {
    this.editorCtx.blur()
    this.updatePosition(0)
    // this.inputFocus = false
    this.setState({
      inputFocus:false
    })
    // this.inputFocus = false
  }
  changeKeyBoard() {
    // this.toolBarContentShow = false
    this.setState({
      toolBarContentShow:false
    })
    // this.toolBarContentShow = false
    this.hideKeyboard()
  }
  hideToolbar() {
    this.hideKeyboard()
    this.setState({
      toolbarShow:false
    },()=>{
      this.scrollViewHeight()
    })
    // this.toolbarShow = false
  }
  // 修改默认样式
  formatDefault(format) {
    for (let name in format) {
      this.editorCtx.format(name, format[name])
    }
    if (format.bold) {
      this.editorCtx.format('bold', true)
    } else if (this.formats.bold) {
      this.editorCtx.format('bold', '')
    }
    this.editorCtx.format('lineHeight', '') //选择默认样式时，取消当前行高的选择
  }
  formatformat(bind, item = {}, pitem = {}) {
    item.name = item.name || pitem.name || ''
    let {
      name,
      value
    } = item
    switch (bind) {
      case 'format': //改变文本样式
        if (!name) return
        if (name == 'defaultFormat') { //选择标题样式时，取消当前字号的选择
          this.formatDefault(item.format)
        } else {
          this.editorCtx.format(name, value)
        }
        break
      case 'removeFormat': //删除字体样式
        this.editorCtx.removeFormat()
        break
      case 'insertDate': //插入时间
        var date = new Date()
        var formatDate = `${date.getFullYear()} 年${date.getMonth() + 1} 月${date.getDate()} 日`;
        this.editorCtx.insertText({
          text: formatDate
        })
        break
      case 'check': //设置当前行为待办列表格式
        this.editorCtx.format('list', 'check')
        break
      case 'undo': //撤销操作
        this.editorCtx.undo()
        break
      case 'redo': //恢复操作
        this.editorCtx.redo()
        break
      case 'insertDivider': //添加分割线
        this.editorCtx.insertDivider()
        break
      case 'clear': //清除内容
        this.editorCtx.clear()
        break
      case 'chooseImage': //插入相册图片
        this.chooseImage()
        break;
      case 'chooseImagebyCamera': //拍摄
        this.chooseImage(true)
        break
    }
  }
  onStatusChange(e) {
    // this.formats = e.detail
    this.setState({
      formats:e.detail
    })
    console.log(this.formats)
  }
  chooseImage(onlyCamera) {

    const that = this
    const _createdAt = Date.now()
   // this.getUid = () => `wux-upload--${this.createdAt}-${++this.index}`
   // this.uploadTask = {}
   // this.tempFilePaths = []
    const success = res => {
      this.tempFilePaths = res.tempFiles.map(item => ({
        url: item.path,
        size: item.size,
        type: item.path.substring(item.path.lastIndexOf('.') + 1, item.path.length),
        uid:  `wux-upload--${_createdAt}-${++that.state.file_index}`
      }))
      // 当前插入图片src地址直接使用临时路径，如果对接接口上传，更改为使用【上传文件】代码片段：

      /* 直接插入临时图片地址 start */

      // this.tempFilePaths.forEach(file => {
      //   this.insertImage(file.url, file)
      // })

      /* 直接插入临时图片地址 end */


      /* 上传文件 start */

      // this.$emit('before', res)
      // this.verifyFile()
      // this.$nextTick(() => {
      // 	this.uploadFile(this.tempFilePaths.length)
      // })
      this.verifyFile()
      this.uploadFile(this.tempFilePaths.length)

      /* 上传文件 end */

    }

    const {
      count,
      sizeType
    } = this
    setTimeout(() => {
      Taro.chooseImage({
        count,
        sizeType,
        //sourceType: onlyCamera ? ['camera'] : this.sourceType,
        sourceType:['album','camera'],
        success
      })
    }, 100)
  }
  insertImage(src, file) {
    var that = this
    that.editorCtx.insertImage({
      src,
      data: {
        id: file.uid
      },
      width: '98%',
       extClass:'editor-img',
      // extClass: 'editor--editor-img', //添加到图片 img标签上的类名为editor-img，设置前缀editor--才生效。部分机型点击图片右边的光标时不灵敏，需将样式editor-img宽度调小 max-width:98%;从而在图片右侧中留出部分位置供用户点击聚集。
      success(e) {
        //真机会自动插入一行空格
      }
    })
  }
  /**
   * 上传文件，支持多图递归上传
   */
  uploadFile(uploadCount, curIndex) {
    let that = this
    if (!this.tempFilePaths.length) return
    const {
      name,
      header,
      formData,
      progress
    } = this
    const file = this.tempFilePaths.shift()
    curIndex ? (file.index = curIndex + 1) : (file.index = 1)
    let {
      uid,
      url: filePath
    } = file
    // if (!url || !filePath) return

    const { site } = this.props;

    console.log(site,'site---')
    const { envConfig } = site;
    const token = locals.get(constants.ACCESS_TOKEN_NAME);

    /*this.uploadTask[uid] = Taro.uploadFile({*/
    Taro.uploadFile({
      url: `${envConfig.COMMON_BASE_URL}/api/v3/attachments`,
      filePath,
      name:'file',
      // header,
      // formData,
      header: {
        'Content-Type': 'multipart/form-data',
        authorization: `Bearer ${token}`,
      },
      formData: {
        type: 1
      },
      success: res => that.onSuccess(file, res),
      fail: res => that.onFail(file, res),
      complete: res => {
        console.log('pppp')
      //  delete this.uploadTask[uid]
      //  this.$emit('complete', res)
        //同时选择多图上传时，只校验第一张图片大小，多图递归上传需逐一校验
        if (!this.tempFilePaths.length) return
        that.verifyFile()
        that.uploadFile(uploadCount, file.index)
      }
    })
    // 判断是否监听上传进度变化
    if (progress) {
     // this.uploadTask[uid].onProgressUpdate(res => this.onProgress(file, res, uploadCount))
    }
  }
  /**校验图片格式和大小是否符合规则 */
  verifyFile() {
    var {
      size: tempFilesSize,
      type
    } = this.tempFilePaths[0] //获取图片的大小，单位B
    // this.state.noAllowType.map(item => {
    //   if (type == item) {
    //     Taro.showToast({
    //       title: `不支持上传${item}图片`,
    //       icon: 'none'
    //     })
    //     this.tempFilePaths.shift()
    //   }
    // })

    if (tempFilesSize / 1024 > this.maxSize * 1024) {
      Taro.showToast({
        title: '上传图片不能大于' + this.bytesToSize(this.maxSize * 1024 * 1024) + '!',
        icon: 'none'
      })
      this.tempFilePaths.shift()
    }
  }
  onSuccess(file, res) {
    //按照接口自行处理数据，insertImage的src参数为接口返回的图片地址
    /**
     * 示例数据:
     res = {
				   	data: '{"code":0,"msg":"上传成功","data":{"path":"https://xxx.com/images/upload/1.png"}}'
				   }
     */
    let json = JSON.parse(res.data)
    console.log(json,'res.data--')
    if (json.Code == 0) {
      this.insertImage(json.Data.url, file)
    } else {
      Taro.showToast({
        title: '图片上传失败.'+json.Message,
        icon: 'none'
      })
    }
  }
  onFail(file, res) {
    Taro.showToast({
      title: '图片上传失败！！',
      icon: 'none'
    })
  }
  /**
   * 监听上传进度变化的回调函数
   * @param {Object} file 文件对象
   * @param {Object} res 请求响应对象
   * @param {Number} uploadCount 选择图片总数量
   */
  onProgress(file, res, uploadCount) {
    if (res.progress != 100) {
      Taro.showToast({
        title: `正在上传图片${file.index}/${uploadCount}`,
        icon: 'none'
      })
    }

    const targetItem = {
      ...file,
      progress: res.progress,
      res
    }
    const info = {
      file: targetItem
    }

    this.$emit('progress', info)
  }
  bytesToSize(bytes) {
    if (bytes === 0) return '0 B'
    var k = 1024,
      sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
      i = Math.floor(Math.log(bytes) / Math.log(k))

    return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i]
  }
  setValue(value) {
    if (this.editorCtx) {
      this.editorCtx.setContents({
        html: value,
        success: () => {
          this.getContents(res => {
            this.onEditorInput({
              detail: {
                html: res.html,
                text: res.text
              }
            })
           // this.$emit('update', res)
          })
        }
      })
    }
  }
  getContents(callback) {
    //由于获取编辑器内容getContents为异步，因此需要使用callback回调
    this.editorCtx.getContents({
      success: res => {
        callback(res)
      }
    })
  }
  save() {
    this.editorCtx.getContents({
      success: res => {
        res.html = handleHtmlImage(res.html, true)
       // this.$emit('save', res)
        locals.set('_editorContent', res.html);
       // let _activityInfoList =  locals.get('_activityInfoList')

       // locals.set('_activityInfoList', {..._activityInfoList,...{contents:res.html}});
        setTimeout(() => {
          // Taro.redirectTo({ url: '/indexPages/CreateActivity/index' })
          Taro.navigateBack()
        }, 1000)

        Taro.showToast({
          title: '保存成功',
          icon: 'none'
        })

      },
      complete: res => {
        console.log('getContents complete')
      }
    })
  }

  render() {
    return (
        <View className="editor-container">
          {/*{`${styles.container} ${show ? '' : styles['is-display']}`}*/}
          <View id="fixed-top" className={`${styles['fixed-top']} ${this.state.isFixed?styles['isFixed']:''}`}>
            <Button className={`${styles.btn} ${styles['btn-primary']}`} onClick={this.save.bind(this)}>保存</Button>
          </View>
          <View className={styles['fixed-top__place']}></View>
          <scroll-view scroll-y style={`height: ${this.state.scrollViewHeight}px`} >
            <Editor id="editor" class={styles['cu-editor']} placeholder={this.state.placeholder}
            showImgSize={this.state.showImgSize} showImgToolbar={this.state.showImgToolbar} showImgResize={this.state.showImgResize}
                   onStatuschange={this.onStatusChange.bind(this)}
              onReady={this.onEditorReady.bind(this)}
              onInput={this.onEditorInput.bind(this)}
              onFocus={this.onEditorFocus.bind(this)}
              onBlur={this.onEditorBlur.bind(this)}></Editor>
           </scroll-view>
          {/*<View className="fixed-bottom" hidden="!toolbarShow" style={ `bottom: ${this.state.fixedBottom}px`} bindtouchstart="">*/}
          <View className={styles['fixed-bottom']}  hidden={!this.state.toolbarShow}  style={ `bottom: ${this.state.fixedBottom}px;display:${this.state.toolbarShow?'block':'none'}`} catchtouchstart="">
            <View className={`${styles.toolbar} ${styles['selector']}`} style={`height: ${this.state.toolBarHeight}rpx`}>
              <View className={styles['toolbar-item-header']} onClick={this.changeKeyBoard.bind(this)}>
                <Text className="iconfont icon-keyboard"></Text>
              </View>
              {
                this.state.iconArray.map((icon,index)=>{
                  return (
                    <View className={styles['toolbar-item']} onClick={this.changeSwiper.bind(this,index)}>
                      <Text className={`"iconfont" ${icon} ${this.state.toolBarContentShow && this.state.swiperCurrent == index ? styles['active']:''}`}></Text>
                    </View>
                  )
                })
              }
            <View className={styles['toolbar-item']} onClick={this.formatformat.bind(this,'check')}>
              <Text className="iconfont icon-list-check"></Text>
            </View>
            <View className={styles['toolbar-item']} onClick={this.formatformat.bind(this,'undo')}>
              <Text className="iconfont icon-undo"></Text>
            </View>
          <View className={styles['toolbar-item']} onClick={this.formatformat.bind(this,'redo')}>
            <Text className="iconfont icon-redo"></Text>
        </View>
        <View className={styles['toolbar-item-footer']} onClick={this.hideToolbar.bind(this)}>
          <Text className="iconfont icon-check"></Text>
        </View>
      </View>
    {/*<swiper hidden="!toolBarContentShow" class="toolbar-content swiper-box"*/}
    <Swiper className={classNames(styles['toolbar-content'], styles['swiper-box'])}
  style={`display:${this.state.toolBarContentShow?'block':'none'};height: ${this.state.toolBarContentHeight}rpx`} current={this.state.swiperCurrent} duration="300">

      {
        this.state.formatArray.map((page,i)=>{
          return (
            <SwiperItem style={{display:'none'}}>
              {
                page.type === 'feature' &&
                <View className={`${styles['feature-items']} ${styles['flex']}`}>
                  {
                    page.array.map((pitem,pindex)=>{
                      return (
                        <View  key={{pindex}} className={`${styles['feature']}`} onClick={this.formatformat.bind(this,pitem.name)}>
                          <View className={`${styles.icon}`}>
                            <Text className={`iconfont icon-${pitem.icon}}`}></Text>
                          </View>
                        </View>
                      )
                    })
                  }
                </View>
              }
              {
                page.type === 'tool' &&
                  <View>
                    {
                      page.array.map((pitem,pindex)=>{
                        return (
                          <View key={{pindex}} className={`${styles['tool-items']} ${styles['flex']}`}>
                            {
                              pitem.items.map((item, index)=>{
                                return (
                                  <View key={{index}} onClick={this.formatformat.bind(this,'format', item, pitem)}
                                        className={`${styles['tool-item']} ${pitem.name == 'color' ?styles['noBgColor']:''} ${this.isActive(item, pitem)?'ql-active':''}`}
                             /* className={ 'ql-active': this.isActive(item, pitem), noBgColor: pitem.name == 'color' }*/>
                                    {
                                      pitem.name == 'color' ?
                                      <View  className={styles['color-circle']}

                                             style={`background-color:${item.value}`}></View>:
                                        pitem.label == 'icon'? <Icon className={`iconfont icon-${item.icon}`}></Icon>:
                                          <Text className={styles.txt}
                                      style={`fontSize: ${pitem.name == 'fontSize' ? item.value : ''}; ${item.style}`}>
                                            {/*"[{fontSize : pitem.name == 'fontSize' ? item.value : ''}, item.style]"*/}
                                            { item.title || item.value }
                                          </Text>
                                    }

                              </View>
                                )
                              })
                            }
                          </View>
                        )
                      })
                    }
                  </View>

              }
            </SwiperItem>
            )

        })
      }


          </Swiper>
        </View>
      </View>
    );
  }
}

export default TaroEditor;
